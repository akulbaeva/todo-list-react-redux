import React from 'react';
import {Show_Active, Show_All, Show_Completed} from "../actions/action.types";
import Button from "./Button";


const Footer = () => (
    <div>
        <Button filter={Show_All}>All</Button>
        <Button filter={Show_Active}>Active</Button>
        <Button filter={Show_Completed}>Completed</Button>
    </div>
)

export default (Footer)