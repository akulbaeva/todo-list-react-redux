import {connect} from 'react-redux';
import {setFilter} from "../actions/actions";
import React from "react";
import todoStyle from "./Todo.module.css";

const Button = (props) => (
    <button
        onClick={props.onClick}
        disabled={props.active}
        className={todoStyle.buttons}>
        {props.children}
    </button>
)

const mapStateToProps = (state, ownProps) => ({
    active: ownProps.filter === state.filterReducer
})

const mapDispatchToProps = (dispatch, ownProps) => ({
    onClick: () => dispatch(setFilter(ownProps.filter))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Button)