import {Show_Active, Show_Completed} from "../actions/action.types";
import {connect} from "react-redux";
import Todo from "./Todo";

const Filtered = (todos, filter) => {
    if (filter === Show_Active) {
        return todos.filter(it => !it.done)
    } else if (filter === Show_Completed) {
        return todos.filter(it => it.done)
    } else {
        return todos
    }
}

const mapStateToProps = state => ({
    todos: Filtered(state.todo, state.filterReducer)
});

export default connect(mapStateToProps)(Todo)