import React from "react";
import '../App.css';
import todoStyle from './Todo.module.css'
import {connect} from 'react-redux'
import {completeTodo, deleteTodo} from "../actions/actions";
import {Clear_All, Clear_Completed} from "../actions/action.types";


const Todo = (props) => {
    return (
        <div style={{color: '#000'}}>
            {props.todos.map(todo => <div className={todoStyle.todoList}
                                          key={todo.id}
                                          onClick={() => props.completeTodo(todo.id)}
                                          style={{
                                              background: todo.done ? '#f5f5f5' : '',
                                              textDecoration: todo.done ? "line-through" : ""
                                          }}
            >
                {todo.input}
                <button onClick={(e) => {
                    props.removeTodo(todo.id)
                    e.stopPropagation()
                }} className={todoStyle.deleteButton}>X
                </button>
            </div>)}

            <div>
                <button className={todoStyle.buttons} onClick={props.clearAll}>Clear all</button>
                <button className={todoStyle.buttons} onClick={props.clearCompleted}>Clear Completed</button>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        todos: state.todo
    }
}

const mapDispatchToProps = dispatch => {
    return {
        removeTodo: id => dispatch(deleteTodo(id)),
        completeTodo: id => dispatch(completeTodo(id)),
        clearAll: () => dispatch({type: Clear_All}),
        clearCompleted: () => dispatch({type: Clear_Completed})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Todo)