import React from "react";
import {connect} from 'react-redux'
import {Add_Todo, Set_Input} from "../actions/action.types";


const InputForm = (props) => {
    return (
        <div>
            <input type="text" placeholder="TO DO"
                   onChange={(e) => {
                       props.handleOnChange(e.target)
                   }}
                   onKeyDown={(e) => {
                       if (e.keyCode === 13) {
                           e.preventDefault()
                           props.submit(props.input)
                       }
                   }}
            />
            <button onClick={() => {
                props.submit(props.input)
            }}>Add</button>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        input: state.input
    }
}

const mapDispatchToProps = dispatch => {
    return {
        handleOnChange: target => {
            dispatch({type: Set_Input, value: target.value})
        },
        submit: input => {
            const obj = {id: Math.random(), input, done: false}
            dispatch({type: Add_Todo, value: obj})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(InputForm);
