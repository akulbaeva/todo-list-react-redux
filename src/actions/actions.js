import {Complete_Todo, Delete_Todo, Set_Filter} from "./action.types";

export const deleteTodo = id => ({
    type: Delete_Todo,
    id
})

export const completeTodo = id => ({
    type: Complete_Todo,
    id
})

export const setFilter = filter => ({
    type: Set_Filter,
    filter
})