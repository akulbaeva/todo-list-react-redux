import {createStore, combineReducers} from 'redux'
import InputReducer from "../reducers/InputReducer";
import TodoReducer from "../reducers/TodoReducer";
import FilterReducer from "../reducers/FilterReducer";

const store = createStore(combineReducers({
    input: InputReducer,
    todo: TodoReducer,
    filterReducer: FilterReducer
}))

export default store