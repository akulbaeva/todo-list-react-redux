import {Set_Filter, Show_All} from "../actions/action.types";

const FilterReducer = (state = Show_All, action) => {
    if (action.type === Set_Filter) {
        return action.filter
    }
    return state
}

export default FilterReducer