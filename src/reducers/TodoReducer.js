import {Add_Todo, Clear_All, Clear_Completed, Complete_Todo, Delete_Todo} from "../actions/action.types";

const TodoReducer = (state = [], action) => {
    if (action.type === Add_Todo) {
        return [...state, action.value]
    } else if (action.type === Complete_Todo) {
        return state.map(todo =>
            todo.id === action.id ? {...todo, done: !todo.done} : todo
        )
    } else if (action.type === Delete_Todo) {
        return state.filter(todo => todo.id !== action.id)
    } else if(action.type === Clear_All){
        return []
    } else if (action.type === Clear_Completed){
        return state.filter(todo => !todo.done)
    }
    return state
}

export default TodoReducer