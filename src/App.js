import React from 'react';
import './App.css';
import InputForm from "./components/InputForm";
import Filtered from "./components/Filtered";
import Footer from "./components/Footer";

function App() {
    return (
        <div className="App ">
            <header className="App-header">
                <InputForm/>
                <Filtered/>
                <Footer/>
            </header>
        </div>
    )
}

export default App;
